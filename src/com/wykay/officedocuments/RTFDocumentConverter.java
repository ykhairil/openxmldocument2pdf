package com.wykay.officedocuments;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.rtf.RTFEditorKit;

public class RTFDocumentConverter {

	
	public byte[] getPDFByteArrayFromRTFByte(byte[] i_byte_array) throws Exception
	{
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ByteArrayInputStream byteInputStream = new ByteArrayInputStream(i_byte_array);
		RTFEditorKit rtfEditorKit = new RTFEditorKit();
		DefaultStyledDocument defaultStyledDocument = new DefaultStyledDocument();
		rtfEditorKit.read(byteInputStream, defaultStyledDocument , 0);
		
		//read element in rtf
		Element root = defaultStyledDocument.getDefaultRootElement();
		int elem_size = root.getElementCount();
		
		//create rudimentary xsl fo
		StringBuilder fop_gen = new StringBuilder();
		fop_gen.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"> <fo:layout-master-set> ");
		
		for (int i=0; i < elem_size; ++i)
		{
			Element line = root.getElement(i);
			int start = line.getStartOffset();
			int end = line.getEndOffset();
			
			//get a length of text in the line
			String text = defaultStyledDocument.getText(start, end-start);
			
			//then for each character, get the font, color and size
			//for wrapping in xml/html tags
			boolean isFontSameAsPrevious=false;
			boolean isColorSameAsPrevious=false;
			
			AttributeSet attributeSet = defaultStyledDocument.getCharacterElement(start).getAttributes();
			
		}
		
		return byteArrayOutputStream.toByteArray();
	}
}
