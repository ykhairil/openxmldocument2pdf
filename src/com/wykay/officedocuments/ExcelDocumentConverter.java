package com.wykay.officedocuments;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.apache.poi.hssf.converter.ExcelToFoConverter;
import org.apache.poi.hssf.converter.ExcelToFoUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.util.XMLHelper;
//import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.SAXException;

public class ExcelDocumentConverter {
	
	public void convertXlsToPDF(String i_doc_path, String i_output_path) throws Exception
	{
		byte[] filebuf = getPDFByteArrayFromXls(i_doc_path);
		FileOutputStream fileout = new FileOutputStream(new File(i_output_path));
		fileout.write(filebuf);
		fileout.close();
		fileout = null;
	}
	
	public void convertXlsxToPDF(String i_doc_path, String i_output_path) throws Exception
	{
		byte[] filebuf = getPDFByteArrayFromXlsx(i_doc_path);
		FileOutputStream fileout = new FileOutputStream(new File(i_output_path));
		fileout.write(filebuf);
		fileout.close();
		fileout = null;
	}
	
	public byte[] getPDFByteArrayFromXls(String i_doc_path) throws Exception
	{
		//Step 1: get FOP structure		
		HSSFWorkbook hssfWorkbook = ExcelToFoUtils.loadXls(new File(i_doc_path));
		
		ExcelToFoConverter excelToFoConverter = new ExcelToFoConverter(XMLHelper.getDocumentBuilderFactory().newDocumentBuilder().newDocument());
		excelToFoConverter.processWorkbook(hssfWorkbook);		    

	    StringWriter stringWriter = new StringWriter();

	    Transformer transformer = TransformerFactory.newInstance().newTransformer();
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.transform(new DOMSource(excelToFoConverter.process(new File(i_doc_path))), new StreamResult(stringWriter));

	    String xslt_result = stringWriter.toString();   
	    
	    //convert to pdf
	    
	    ByteArrayInputStream fopbyte = new ByteArrayInputStream(xslt_result.getBytes(Charset.defaultCharset()));
	    
	    FopFactory fopFactory = null;
    	
	    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
	    
	    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
	    else
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
	    
	    OutputStream out = null;	    
	    
	    //Step 2: setup stream, byte if it should not be saved, or fileout if it should
	    
	    out = new ByteArrayOutputStream();
	    

	 // Step 3: Construct fop with desired output format
	    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
	    

	    // Step 4: Setup JAXP using identity transformer
	    //TransformerFactory factory = TransformerFactory.newInstance();
	    //Transformer transformer = factory.newTransformer(); // identity transformer

	    // Step 5: Setup input and output for XSLT transformation
	    // Setup input stream
	    
	    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
	    Source src = new StreamSource(fopbyte);

	    // Resulting SAX events (the generated FO) must be piped through to FOP
	    Result res = new SAXResult(fop.getDefaultHandler());

	    // Step 6: Start XSLT transformation and FOP processing
	    transformer.transform(src, res);
	    
	    out.close();
	    
	    System.out.println("Finished output");
	    
	    ByteArrayOutputStream output = (ByteArrayOutputStream)out;
	    stringWriter = null;
	    fopbyte = null;
	    fopFactory = null;
	    src = null;
	    res = null;
	    return output.toByteArray();
	    
		
	}
	
	public byte[] getPDFByteArrayFromXlsByteArray(byte[] i_byte_array) throws Exception
	{
		byte[] output = null;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<byte[]> future = executor.submit(new CallableXlsByteArrayToPDFByteArray(i_byte_array));
		try
		{
			output = future.get(120, TimeUnit.SECONDS);
		}
		catch(TimeoutException e)
		{
			System.out.println("getPDFByteArrayFromXlsByteArray timed out");
			future.cancel(true);
		}
		finally
		{
			executor.shutdownNow();
		}
		
		
		return output;    
		
	}
	
	public byte[] getPDFByteArrayFromXlsx(String i_doc_path) throws Exception
	{
		//xml conversion couldnt work out so i had to make my own xsl-fo
		//based on spreadsheet data
		//reference: http://webapp.docx4java.org/OnlineDemo/ecma376/SpreadsheetML/index.html
		
		FileInputStream file = new FileInputStream(new File(i_doc_path));            

		XSSFWorkbook workbook = new XSSFWorkbook(file); 
		
		//create rudimentary xsl fo
		StringBuilder fop_gen = new StringBuilder();
		fop_gen.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"> <fo:layout-master-set> ");
		
		//create page setup master fo
		int pagesetupcounter=1;
		for (int i=0; i < workbook.getNumberOfSheets(); ++i)
		{
			fop_gen.append("<fo:simple-page-master master-name=");
			//if (pgsize.width > pgsize.height)
				fop_gen.append("\"A4-landscape-"+pagesetupcounter+ "\" page-height=\"21cm\" page-width=\"29.7cm\">");
			//else
			//	fop_gen.append("\"A4-portrait-" +pagesetupcounter+ "\" page-height=\"29.7cm\" page-width=\"21cm\">");
			fop_gen.append("<fo:region-body region-name=\"region-" + pagesetupcounter + "\"> </fo:region-body> </fo:simple-page-master>");
			++pagesetupcounter;
		}
		fop_gen.append("</fo:layout-master-set> ");		
		
		//create actual pages per setup
		int idx = 1;		
		for (int i=0; i < workbook.getNumberOfSheets(); ++i)
		{
			fop_gen.append("\n<fo:page-sequence master-reference=\"A4-landscape-");
			fop_gen.append(idx);
			//fop_gen.append("\"> <fo:static-content flow-name=\"region-");
			//fop_gen.append(idx);
			fop_gen.append("\"> <fo:flow flow-name=\"region-");
			fop_gen.append(idx);
			//fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
			fop_gen.append("\"> <fo:block text-align=\"center\"> ");
			
			
			
			Sheet sheet = workbook.getSheetAt(i);
			Iterator<org.apache.poi.ss.usermodel.Row> rows = sheet.rowIterator();
			//count row for this sheet for a while to set column headers
			fop_gen.append(" <fo:table> ");
			/*while (rows.hasNext())
			{
				org.apache.poi.ss.usermodel.Row row = rows.next();
				for (org.apache.poi.ss.usermodel.Cell cell : row)
				{
					++table_col;
					fop_gen.append(" <fo:table-column column-width=\"2mm\"/>");
				}
			}*/			
			
			fop_gen.append("\n <fo:table-body> ");
			boolean hasAtLeastOneRow=false;
            while (rows.hasNext()) {
            	hasAtLeastOneRow=true;
            	fop_gen.append("\n\t <fo:table-row> ");
                XSSFRow row = (XSSFRow) rows.next();
                for (Cell cell : row) {
                	//XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();
                	//XSSFCell xssfcell = (XSSFCell)cell;
                	XSSFColor cell_bg=XSSFColor.toXSSFColor(cell.getCellStyle().getFillBackgroundColorColor());
                	XSSFColor cell_fg=XSSFColor.toXSSFColor(cell.getCellStyle().getFillForegroundColorColor());
                	
                	String value=null;
                	if (cell.getCellTypeEnum()==CellType.NUMERIC)
                	{
                		//System.out.print(", " + cell.getNumericCellValue());
                		value = Double.toString(cell.getNumericCellValue());
                	}
                	else if (cell.getCellTypeEnum()==CellType.STRING)
                	{
                		//System.out.print(", " + cell.getStringCellValue());
                		value = cell.getStringCellValue();
                	}
                	else if (cell.getCellTypeEnum()==CellType.FORMULA)
                	{
                		//System.out.print( ", " + cell.getNumericCellValue());
                		value = Double.toString(cell.getNumericCellValue());
                	}
                	
                	if (cell_bg!=null)
                	{
                		fop_gen.append(" \n\t\t<fo:table-cell border=\"dotted\" background-color=\"#");
                		
                		String argb = cell_fg.getARGBHex();
                		
                		fop_gen.append(argb.substring(2, 8));
                	}
                	else
                	{
                		fop_gen.append(" \n\t\t<fo:table-cell border=\"dotted");
                		
                	}
                	fop_gen.append("\"> <fo:block> ");
                	fop_gen.append(value);
                	fop_gen.append(" </fo:block> \n\t\t</fo:table-cell> ");
                    }
                //System.out.println("");
                fop_gen.append("\n\t </fo:table-row>");
                }
            if (hasAtLeastOneRow==false)
            {
            	//empty sheet
            	fop_gen.append("\n\t <fo:table-row> ");
            	fop_gen.append(" \n\t\t<fo:table-cell border=\"dotted\">");
            	fop_gen.append("\n\t\t\t <fo:block> ");
            	fop_gen.append(" ");
            	fop_gen.append(" \n\t\t\t</fo:block>");
            	fop_gen.append(" \n\t\t</fo:table-cell>");
            	fop_gen.append("\n\t </fo:table-row>");
            }
            
			//fop_gen.append(" <fo:table-row> <fo:table-cell> <fo:block>Volvo</fo:block> </fo:table-cell> </fo:table-row> ");
			fop_gen.append(" </fo:table-body> </fo:table>");
			//fop_gen.append(base64img);
			fop_gen.append("</fo:block> </fo:flow> </fo:page-sequence>");
		
		}
		
		fop_gen.append("</fo:root>");
		
		//convert to pdf
	    FileOutputStream tempfile = new FileOutputStream(new File(i_doc_path + ".fo"));
	    tempfile.write(fop_gen.toString().getBytes(Charset.defaultCharset()), 0, fop_gen.toString().getBytes().length);
	    tempfile.close();
	    ByteArrayInputStream fopbyte = new ByteArrayInputStream(fop_gen.toString().getBytes(Charset.defaultCharset()));
	    
	    //FopFactory fopFactory = FopFactory.newInstance(new File("fop.xconf"));
	    FopFactory fopFactory = null;
    	
	    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
	    
	    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
	    else
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
	    
	    OutputStream out = null;	    
	    
	    //Step 2: setup stream, byte if it should not be saved, or fileout if it should
	    
	    out = new ByteArrayOutputStream();
	    

	    // Step 3: Construct fop with desired output format
	    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
	    

	    // Step 4: Setup JAXP using identity transformer
	    TransformerFactory factory = TransformerFactory.newInstance();
	    Transformer transformer = factory.newTransformer(); // identity transformer

	    // Step 5: Setup input and output for XSLT transformation
	    // Setup input stream
	    
	    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
	    Source src = new StreamSource(fopbyte);

	    // Resulting SAX events (the generated FO) must be piped through to FOP
	    Result res = new SAXResult(fop.getDefaultHandler());

	    // Step 6: Start XSLT transformation and FOP processing
	    transformer.transform(src, res);
	    
	    out.close();
	    workbook.close();
	    System.out.println("Finished output");
	    fop_gen.delete(0, fop_gen.length()-1);
	    res = null;
	    src=null;
	    fopbyte = null;
	    tempfile = null;
	    fop_gen = null;
	    workbook = null;
	    file=null;
	    
	    
	    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
	    return output.toByteArray();
	    
		
	}
	
	public byte[] getPDFByteArrayFromXlsxByteArray(byte[] i_byte_array) throws Exception
	{
		//xml conversion couldnt work out so i had to make my own xsl-fo
		//based on spreadsheet data
		//reference: http://webapp.docx4java.org/OnlineDemo/ecma376/SpreadsheetML/index.html
		byte[] output = null;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<byte[]> future = executor.submit(new CallableXlsxByteArrayToPDFByteArray(i_byte_array));
		try
		{
			output = future.get(120, TimeUnit.SECONDS);
		}
		catch(TimeoutException e)
		{
			System.out.println("getPDFByteArrayFromXlsxByteArray timed out");
			future.cancel(true);
		}
		
		executor.shutdownNow();
		return output;	    
		
	}
	
	class CallableXlsByteArrayToPDFByteArray implements Callable<byte[]>
	{
		byte[] m_input;
		public CallableXlsByteArrayToPDFByteArray(byte[] i_input)
		{
			m_input = i_input;
		}
		
		@Override
		public byte[] call() throws Exception
		{
			//Step 1: get FOP structure
			File tempfile = File.createTempFile("odc-xls-gen", ".tmp");
			
			FileOutputStream fos = new FileOutputStream(tempfile);
			fos.write(m_input);
			fos.close();
			
			HSSFWorkbook hssfWorkbook = ExcelToFoUtils.loadXls(tempfile);
			
			ExcelToFoConverter excelToFoConverter = new ExcelToFoConverter(XMLHelper.getDocumentBuilderFactory().newDocumentBuilder().newDocument());
			excelToFoConverter.processWorkbook(hssfWorkbook);			    

		    StringWriter stringWriter = new StringWriter();

		    Transformer transformer = TransformerFactory.newInstance().newTransformer();
		    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		    transformer.transform(new DOMSource(excelToFoConverter.process(tempfile)), new StreamResult(stringWriter));

		    String xslt_result = stringWriter.toString();   
		    tempfile.delete();
		    //convert to pdf
		    
		    ByteArrayInputStream fopbyte = new ByteArrayInputStream(xslt_result.getBytes(Charset.defaultCharset()));
		    
		    FopFactory fopFactory = null;
	    	
		    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
		    
		    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
		    else
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
		    
		    OutputStream out = null;	    
		    
		    //Step 2: setup stream, byte if it should not be saved, or fileout if it should
		    
		    out = new ByteArrayOutputStream();
		    

		 // Step 3: Construct fop with desired output format
		    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
		    

		    // Step 4: Setup JAXP using identity transformer
		    //TransformerFactory factory = TransformerFactory.newInstance();
		    //Transformer transformer = factory.newTransformer(); // identity transformer

		    // Step 5: Setup input and output for XSLT transformation
		    // Setup input stream
		    
		    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
		    Source src = new StreamSource(fopbyte);

		    // Resulting SAX events (the generated FO) must be piped through to FOP
		    Result res = new SAXResult(fop.getDefaultHandler());

		    // Step 6: Start XSLT transformation and FOP processing
		    transformer.transform(src, res);
		    
		    out.close();
		    
		    System.out.println("Finished output");
		    stringWriter = null;
		    fopbyte = null;
		    fopFactory = null;
		    src = null;
		    res = null;
		    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
		    return output.toByteArray();
		}
	}
	
	class CallableXlsxByteArrayToPDFByteArray implements Callable<byte[]>
	{
		private byte[] m_input;
		
		public CallableXlsxByteArrayToPDFByteArray(byte[] i_input)
		{
			m_input = i_input;
		}
		
		@Override
		public byte[] call() throws Exception
		{
			//xml conversion couldnt work out so i had to make my own xsl-fo
			//based on spreadsheet data
			//reference: http://webapp.docx4java.org/OnlineDemo/ecma376/SpreadsheetML/index.html
			
			ByteArrayInputStream file = new ByteArrayInputStream(m_input);            

			XSSFWorkbook workbook = new XSSFWorkbook(file); 
			
			//create rudimentary xsl fo
			StringBuilder fop_gen = new StringBuilder();
			fop_gen.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"> <fo:layout-master-set> ");
			
			//create page setup master fo
			int pagesetupcounter=1;
			for (int i=0; i < workbook.getNumberOfSheets(); ++i)
			{
				fop_gen.append("<fo:simple-page-master master-name=");
				//if (pgsize.width > pgsize.height)
					fop_gen.append("\"A4-landscape-"+pagesetupcounter+ "\" page-height=\"21cm\" page-width=\"29.7cm\">");
				//else
				//	fop_gen.append("\"A4-portrait-" +pagesetupcounter+ "\" page-height=\"29.7cm\" page-width=\"21cm\">");
				fop_gen.append("<fo:region-body region-name=\"region-" + pagesetupcounter + "\"> </fo:region-body> </fo:simple-page-master>");
				++pagesetupcounter;
			}
			fop_gen.append("</fo:layout-master-set> ");		
			
			//create actual pages per setup
			int idx = 1;		
			for (int i=0; i < workbook.getNumberOfSheets(); ++i)
			{
				fop_gen.append("<fo:page-sequence master-reference=\"A4-landscape-");
				fop_gen.append(idx);
				//fop_gen.append("\"> <fo:static-content flow-name=\"region-");
				//fop_gen.append(idx);
				fop_gen.append("\"> <fo:flow flow-name=\"region-");
				fop_gen.append(idx);
				//fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
				fop_gen.append("\"> <fo:block text-align=\"center\"> ");
				
				
				
				Sheet sheet = workbook.getSheetAt(i);
				Iterator<org.apache.poi.ss.usermodel.Row> rows = sheet.rowIterator();
				//count row for this sheet for a while to set column headers
				fop_gen.append(" <fo:table> ");
				/*while (rows.hasNext())
				{
					org.apache.poi.ss.usermodel.Row row = rows.next();
					for (org.apache.poi.ss.usermodel.Cell cell : row)
					{
						++table_col;
						fop_gen.append(" <fo:table-column column-width=\"2mm\"/>");
					}
				}*/			
				
				fop_gen.append(" <fo:table-body> ");
				boolean hasAtLeastOneRow=false;
	            while (rows.hasNext()) {
	            	hasAtLeastOneRow=true;
	            	fop_gen.append(" <fo:table-row> ");
	                XSSFRow row = (XSSFRow) rows.next();
	                for (Cell cell : row) {
	                	//XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();
	                	//XSSFCell xssfcell = (XSSFCell)cell;
	                	XSSFColor cell_bg=XSSFColor.toXSSFColor(cell.getCellStyle().getFillBackgroundColorColor());
	                	XSSFColor cell_fg=XSSFColor.toXSSFColor(cell.getCellStyle().getFillForegroundColorColor());
	                	
	                	String value=null;
	                	if (cell.getCellTypeEnum()==CellType.NUMERIC)
	                	{
	                		//System.out.print(", " + cell.getNumericCellValue());
	                		value = Double.toString(cell.getNumericCellValue());
	                	}
	                	else if (cell.getCellTypeEnum()==CellType.STRING)
	                	{
	                		//System.out.print(", " + cell.getStringCellValue());
	                		value = cell.getStringCellValue();
	                	}
	                	else if (cell.getCellTypeEnum()==CellType.FORMULA)
	                	{
	                		//System.out.print( ", " + cell.getNumericCellValue());
	                		value = Double.toString(cell.getNumericCellValue());
	                	}
	                	
	                	if (cell_bg!=null)
	                	{
	                		fop_gen.append(" <fo:table-cell border=\"dotted\" background-color=\"#");
	                		
	                		String argb = cell_fg.getARGBHex();
	                		
	                		fop_gen.append(argb.substring(2, 8));
	                	}
	                	else
	                	{
	                		fop_gen.append(" <fo:table-cell border=\"dotted");
	                		
	                	}
	                	fop_gen.append("\"> <fo:block> ");
	                	fop_gen.append(value);
	                	fop_gen.append(" </fo:block> </fo:table-cell> ");
	                    }
	                //System.out.println("");
	                fop_gen.append(" </fo:table-row>");
	                }
	            if (hasAtLeastOneRow==false)
	            {
	            	//empty sheet
	            	fop_gen.append("\n\t <fo:table-row> ");
	            	fop_gen.append(" \n\t\t<fo:table-cell border=\"dotted\">");
	            	fop_gen.append("\n\t\t\t <fo:block> ");
	            	fop_gen.append(" ");
	            	fop_gen.append(" \n\t\t\t</fo:block>");
	            	fop_gen.append(" \n\t\t</fo:table-cell>");
	            	fop_gen.append("\n\t </fo:table-row>");
	            }
	            
				//fop_gen.append(" <fo:table-row> <fo:table-cell> <fo:block>Volvo</fo:block> </fo:table-cell> </fo:table-row> ");
				fop_gen.append(" </fo:table-body> </fo:table>");
				//fop_gen.append(base64img);
				fop_gen.append("</fo:block> </fo:flow> </fo:page-sequence>");
			
			}
			
			fop_gen.append("</fo:root>");
			
			//convert to pdf
		    //FileOutputStream tempfile = new FileOutputStream(new File("temp" + ".fo"));
		    //tempfile.write(fop_gen.toString().getBytes(Charset.defaultCharset()), 0, fop_gen.toString().getBytes().length);
		    //tempfile.close();
		    ByteArrayInputStream fopbyte = new ByteArrayInputStream(fop_gen.toString().getBytes(Charset.defaultCharset()));
		    
		    //FopFactory fopFactory = FopFactory.newInstance(new File("fop.xconf"));
		    FopFactory fopFactory = null;
	    	
		    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
		    
		    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
		    else
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
		    
		    OutputStream out = null;	    
		    
		    //Step 2: setup stream, byte if it should not be saved, or fileout if it should
		    
		    out = new ByteArrayOutputStream();
		    

		    // Step 3: Construct fop with desired output format
		    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
		    

		    // Step 4: Setup JAXP using identity transformer
		    TransformerFactory factory = TransformerFactory.newInstance();
		    Transformer transformer = factory.newTransformer(); // identity transformer

		    // Step 5: Setup input and output for XSLT transformation
		    // Setup input stream
		    
		    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
		    Source src = new StreamSource(fopbyte);

		    // Resulting SAX events (the generated FO) must be piped through to FOP
		    Result res = new SAXResult(fop.getDefaultHandler());

		    // Step 6: Start XSLT transformation and FOP processing
		    transformer.transform(src, res);
		    
		    out.close();
		    workbook.close();
		    System.out.println("Finished output");
		    fop_gen.delete(0, fop_gen.length()-1);
		    res = null;
		    src=null;
		    fopbyte = null;
		    //tempfile = null;
		    fop_gen = null;
		    workbook = null;
		    file=null;
		    
		    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
		    return output.toByteArray();
		}
	}

	
}
