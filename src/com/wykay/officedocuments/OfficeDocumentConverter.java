package com.wykay.officedocuments;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
//import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
//import java.nio.file.Path;
//import java.nio.file.Paths;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.omg.PortableServer.Servant;


public class OfficeDocumentConverter {
	
	public static void main(String[] args) 
	{
		try
		{
			OfficeDocumentConverter ofc = new OfficeDocumentConverter();
			if (args.length == 0)
			{
				ofc.showUsage();
			}
			else if (args.length < 2)
			{
				ofc.showError();
				ofc.wordunittest();	
				ofc.excelunittest();
				ofc.powerpointunittest();
			}
			else if (args.length == 4)
			{
				String operation_type = args[0];
				if (operation_type.compareTo("-b")==0)
				{
					//convert bytes
					
					ofc.processRemoteFileTCP(args[1], args[2], args[3]);
				}
				else if (operation_type.compareTo("-f")==0)
				{
					//convert physical file
					ofc.startConvert(args[1], args[2]);
				}
			}
			else
			{
				//ofc.startConvert(args[0], args[1]);

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
			
	}
	
	public void wordunittest() throws Exception
	{
		WordDocumentConverter wordconverter = new WordDocumentConverter();
		wordconverter.convertDocToPDF("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.doc", "C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.doc.pdf");
		byte[] file1buf = wordconverter.getPDFByteArrayFromDoc("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.doc");
		//check if file exists
		File file1 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.doc.pdf");
						
		if (file1.exists() && file1.length() > 8)
			System.out.println("DOC to PDF file OK");
		else
			System.out.println("DOC to PDF file NOT OK");
		if (file1buf.length > 8)
			System.out.println("DOC to PDF byte OK");
		else
			System.out.println("DOC to PDF byte NOT OK");
		
		wordconverter.convertDocxToPDF("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.docx", "C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.docx.pdf");
		byte[] file2buf = wordconverter.getPDFByteArrayFromDocx("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.docx");
		//check if file exists
		File file2 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.docx.pdf");
						
		if (file2.exists() && file2.length() > 8)
			System.out.println("DOCX to PDF file OK");
		else
			System.out.println("DOCX to PDF file NOT OK");
		if (file2buf.length > 8)
			System.out.println("DOCX to PDF byte OK");
		else
			System.out.println("DOCX to PDF byte NOT OK");
		
		File file3 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.doc");
		byte file3read[] = new byte[(int) file3.length()];
		FileInputStream file3stream = new FileInputStream(file3);
		file3stream.read(file3read);
		file3stream.close();
		byte[] file3buf = wordconverter.getPDFByteArrayFromDocByteArray(file3read);
		if (file3buf.length > 8)
			System.out.println("DOC byte to PDF byte OK");
		else
			System.out.println("DOC byte to PDF byte NOT OK");
		
		File file4 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.docx");
		byte file4read[] = new byte[(int) file4.length()];
		FileInputStream file4stream = new FileInputStream(file4);
		file4stream.read(file4read);
		file4stream.close();
		byte[] file4buf = wordconverter.getPDFByteArrayFromDocxByteArray(file4read);
		if (file4buf.length > 8)
			System.out.println("DOCX byte to PDF byte OK");
		else
			System.out.println("DOCX byte to PDF byte NOT OK");
	}
	
	public void excelunittest() throws Exception
	{
		ExcelDocumentConverter excelconverter = new ExcelDocumentConverter();
		excelconverter.convertXlsToPDF("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xls", "C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xls.pdf");
		byte[] file1buf = excelconverter.getPDFByteArrayFromXls("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xls");
		//check if file exists
		File file1 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xls.pdf");
		if (file1.exists() && file1.length() > 8)
			System.out.println("XLS to PDF file OK");
		else
			System.out.println("XLS to PDF file NOT OK");
		if (file1buf.length > 8)
			System.out.println("XLS to PDF byte OK");
		else
			System.out.println("XLS to PDF byte NOT OK");
		
		excelconverter.convertXlsxToPDF("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\imac_gu_dec.xlsx", "C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xlsx.pdf");
		byte[] file2buf = excelconverter.getPDFByteArrayFromXlsx("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xlsx");
		//check if file exists
		File file2 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xlsx.pdf");
		if (file2.exists() && file2.length() > 8)
			System.out.println("XLSX to PDF file OK");
		else
			System.out.println("XLSX to PDF file NOT OK");
		if (file2buf.length > 8)
			System.out.println("XLSX to PDF byte OK");
		else
			System.out.println("XLSX to PDF byte NOT OK");
		
		File file3 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xls");
		byte file3read[] = new byte[(int) file3.length()];
		FileInputStream file3stream = new FileInputStream(file3);
		file3stream.read(file3read);
		file3stream.close();
		byte[] file3buf = excelconverter.getPDFByteArrayFromXlsByteArray(file3read);
		if (file3buf.length > 8)
			System.out.println("XLS byte to PDF byte OK");
		else
			System.out.println("XLS byte to PDF byte NOT OK");
		
		File file4 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.xlsx");
		byte file4read[] = new byte[(int) file4.length()];
		FileInputStream file4stream = new FileInputStream(file4);
		file4stream.read(file4read);
		file4stream.close();
		byte[] file4buf = excelconverter.getPDFByteArrayFromXlsxByteArray(file4read);
		if (file4buf.length > 8)
			System.out.println("XLSX byte to PDF byte OK");
		else
			System.out.println("XLSX byte to PDF byte NOT OK");
	}
	
	public void powerpointunittest() throws Exception
	{
		PowerpointDocumentConverter powerpointconverter = new PowerpointDocumentConverter();
		powerpointconverter.convertPptToPDF("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.ppt", "C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.ppt.pdf");
		byte[] file1buf = powerpointconverter.getPDFByteArrayFromPpt("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.ppt");
		//check if file exists
		File file1 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.ppt.pdf");
		if (file1.exists() && file1.length() > 8)
			System.out.println("PPT to PDF file OK");
		else
			System.out.println("PPT to PDF file NOT OK");
		if (file1buf.length > 8)
			System.out.println("PPT to PDF byte OK");
		else
			System.out.println("PPT to PDF byte NOT OK");
		
		powerpointconverter.convertPptxToPDF("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.pptx", "C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.pptx.pdf");
		byte[] file2buf = powerpointconverter.getPDFByteArrayFromPptx("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.pptx");
		//check if file exists
		File file2 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.pptx.pdf");
		if (file2.exists() && file2.length() > 8)
			System.out.println("PPTX to PDF file OK");
		else
			System.out.println("PPTX to PDF file NOT OK");
		if (file2buf.length > 8)
			System.out.println("PPTX to PDF byte OK");
		else
			System.out.println("PPTX to PDF byte NOT OK");
		
		File file3 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.ppt");
		byte file3read[] = new byte[(int) file3.length()];
		FileInputStream file3stream = new FileInputStream(file3);
		file3stream.read(file3read);
		file3stream.close();
		byte[] file3buf = powerpointconverter.getPDFByteArrayFromPptByteArray(file3read);
		if (file3buf.length > 8)
			System.out.println("PPT byte to PDF byte OK");
		else
			System.out.println("PPT byte to PDF byte NOT OK");
		
		File file4 = new File("C:\\Users\\YUSSAIRI-LAPTOP\\Documents\\odctest\\odctest.pptx");
		byte file4read[] = new byte[(int) file4.length()];
		FileInputStream file4stream = new FileInputStream(file4);
		file4stream.read(file4read);
		file4stream.close();
		byte[] file4buf = powerpointconverter.getPDFByteArrayFromPptxByteArray(file4read);
		if (file4buf.length > 8)
			System.out.println("PPTX byte to PDF byte OK");
		else
			System.out.println("PPTX byte to PDF byte NOT OK");
	}
	
	public void showUsage()
	{
		System.out.println("Office Document Converter 1.2 �2017 Yussairi Khairil");
		System.out.println(" ");
		System.out.println("Usage: OfficeDocumentConverter [operation type] [arg1] [arg2] [arg3]");
		System.out.println(" [operation type]		-b 	: 	binary data operation over UDP");
		System.out.println(" 						-f 	:	file operation on disk");
		System.out.println(" ");
		System.out.println("sourcepath	-	path of Office file to convert");
		System.out.println("destinationpath	-	path of PDF file to save as");
	}
	
	public void showError()
	{
		System.out.println("Not enough argument");
	}
	
	public void startConvert(String src, String dest) throws Exception
	{
		//JSE1.8 stuffs
		//Path path = Paths.get(src);
		//String ext = path.getName(path.getNameCount()-1).toString();
		File path = new File(src);
		String fileparts[] = path.getName().split("[.]");
		String ext = null;
		if (fileparts.length > 1)
			ext = fileparts[1];
		if (ext.contains("docx"))
		{
			WordDocumentConverter docconv=new WordDocumentConverter();
			docconv.convertDocxToPDF(src, dest);
		}
		else if (ext.contains("doc"))
		{
			WordDocumentConverter docconv=new WordDocumentConverter();
			docconv.convertDocToPDF(src, dest);
		}
		else if (ext.contains("xlsx"))
		{
			ExcelDocumentConverter xlsconv=new ExcelDocumentConverter();
			xlsconv.convertXlsxToPDF(src, dest);
		}
		else if (ext.contains("xls"))
		{
			ExcelDocumentConverter xlsconv=new ExcelDocumentConverter();
			xlsconv.convertXlsToPDF(src, dest);
		}
		else if (ext.contains("pptx"))
		{
			PowerpointDocumentConverter pptconv =new PowerpointDocumentConverter();
			pptconv.convertPptxToPDF(src, dest);
		}
		else if (ext.contains("ppt"))
		{
			PowerpointDocumentConverter pptconv =new PowerpointDocumentConverter();
			pptconv.convertPptToPDF(src, dest);
		}
	}
	
	void processRemoteFileTCP(String office_format, String arg_port, String arg_size_bytes)
	{
		try
		{
			int port = Integer.parseInt(arg_port);
			int sizebyte = Integer.parseInt(arg_size_bytes);
			int readbyte = 0;
			ServerSocket serverSocket = new ServerSocket(port);
			byte[] receiveData = new byte[sizebyte];
			System.out.printf("Listening on tcp:%s:%d%n",
	                InetAddress.getLocalHost().getHostAddress(), port);
			
			//accept incoming connection
			Socket connectionSocket = serverSocket.accept();
			System.out.printf("Client:%s%n",
	                connectionSocket.getInetAddress().getHostAddress());
			
			//get data
			//connectionSocket.getOutputStream().write(receiveData);
			int read=0;
			ByteArrayOutputStream dataoutput = new ByteArrayOutputStream();
			/*while ((read = connectionSocket.getInputStream().read(receiveData, 0, sizebyte))!=-1)
			{
				
				readbyte+=read;
				if (readbyte >= sizebyte)
					break;
				
			}*/
			
			byte [] buffer = new byte[8192];
			/*while ((read=connectionSocket.getInputStream().read(buffer))!=-1)
			{
				
				dataoutput.write(buffer, 0, read);
				readbyte+=read;
				if (readbyte >= sizebyte || read == -1)
					break;
			}*/
			
			
			BufferedInputStream datainput = new BufferedInputStream(connectionSocket.getInputStream());
			while ((read = datainput.read(buffer))>0)
			{
				dataoutput.write(buffer, 0, read);
				readbyte+=read;
				//dataoutput.flush();
				if (readbyte >= sizebyte)
					break;
			}
			
			
			/*DataInputStream datainput = new DataInputStream(connectionSocket.getInputStream());
			while ((read = datainput.read(buffer, 0, sizebyte))>0)
			{
				dataoutput.write(buffer, 0, read);
				readbyte+=read;
			}*/
			
			
			
			System.out.printf("Expected: %d bytes. Received : %d bytes%n",
	                sizebyte, readbyte);
			
			
			byte[] convertedData=null;
			
	        if (office_format.compareTo("doc")==0)
	        {
	        	WordDocumentConverter docconv = new WordDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromDocByteArray(dataoutput.toByteArray());	        	
	        }
	        else if (office_format.compareTo("docx")==0)
	        {
	        	WordDocumentConverter docconv = new WordDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromDocxByteArray(dataoutput.toByteArray());	        	
	        }
	        else if (office_format.compareTo("xls")==0)
	        {
	        	ExcelDocumentConverter docconv = new ExcelDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromXlsByteArray(dataoutput.toByteArray());	        	
	        }
	        else if (office_format.compareTo("xlsx")==0)
	        {
	        	ExcelDocumentConverter docconv = new ExcelDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromXlsxByteArray(dataoutput.toByteArray());	        	
	        }
	        else if (office_format.compareTo("ppt")==0)
	        {
	        	PowerpointDocumentConverter docconv = new PowerpointDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromPptByteArray(dataoutput.toByteArray());	        	
	        }
	        else if (office_format.compareTo("pptx")==0)
	        {
	        	PowerpointDocumentConverter docconv = new PowerpointDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromPptxByteArray(dataoutput.toByteArray());	        	
	        }
	        else
	        {
	        	convertedData = "deadbeef".getBytes("UTF-8");
	        }
	        
	        System.out.printf("Generated: %d bytes%n",
	                convertedData.length);
	        //send result back to sender	        
	        connectionSocket.getOutputStream().write(convertedData, 0, convertedData.length);
	        connectionSocket.close();
	        serverSocket.close();
			 
			
		}
		catch (Exception e)
		{
			e.printStackTrace(System.out);
			
		}
		
	}
	
	void processRemoteFileUDP(String office_format, String arg_port, String arg_size_bytes)
	{
		//receive data from IP
		try
		{
			int port = Integer.parseInt(arg_port);
			int sizebyte = Integer.parseInt(arg_size_bytes);
			DatagramSocket serverSocket = new DatagramSocket(port);
			byte[] receiveData = new byte[sizebyte];			

	        System.out.printf("Listening on udp:%s:%d%n",
	                InetAddress.getLocalHost().getHostAddress(), port);     
	        DatagramPacket receivePacket = new DatagramPacket(receiveData,
	                           receiveData.length);
	        
	        //make a receiver loop
	        
	        serverSocket.receive(receivePacket);
	        //String sentence = new String( receivePacket.getData(), 0,
	        //                         receivePacket.getLength() );
	          
	        byte[] convertedData=null;
	        if (office_format.compareTo("doc")==0)
	        {
	        	WordDocumentConverter docconv = new WordDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromDocByteArray(receivePacket.getData());	        	
	        }
	        else if (office_format.compareTo("docx")==0)
	        {
	        	WordDocumentConverter docconv = new WordDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromDocxByteArray(receivePacket.getData());	        	
	        }
	        else if (office_format.compareTo("xls")==0)
	        {
	        	ExcelDocumentConverter docconv = new ExcelDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromXlsByteArray(receivePacket.getData());	        	
	        }
	        else if (office_format.compareTo("xlsx")==0)
	        {
	        	ExcelDocumentConverter docconv = new ExcelDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromXlsxByteArray(receivePacket.getData());	        	
	        }
	        else if (office_format.compareTo("ppt")==0)
	        {
	        	PowerpointDocumentConverter docconv = new PowerpointDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromPptByteArray(receivePacket.getData());	        	
	        }
	        else if (office_format.compareTo("pptx")==0)
	        {
	        	PowerpointDocumentConverter docconv = new PowerpointDocumentConverter();
	        	convertedData = docconv.getPDFByteArrayFromPptxByteArray(receivePacket.getData());	        	
	        }
	        else
	        {
	        	convertedData = "deadbeef".getBytes("UTF-8");
	        }
	        
	       
        	// now send acknowledgement packet back to sender     
            InetAddress IPAddress = receivePacket.getAddress();
            //String sendString = "polo";
            //byte[] sendData = sendString.getBytes("UTF-8");
            DatagramPacket sendPacket = new DatagramPacket(convertedData, convertedData.length,
                   IPAddress, receivePacket.getPort());
            serverSocket.send(sendPacket);
            serverSocket.close();
	        
		}
		catch (Exception e)
		{
			e.printStackTrace(System.out);
		}
		finally
		{
			
		}
		
		
		//convert
		
		//return data to IP
	}

}
