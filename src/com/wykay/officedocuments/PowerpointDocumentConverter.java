package com.wykay.officedocuments;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.DatatypeConverter;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.apache.poi.hslf.usermodel.HSLFSlide;
import org.apache.poi.hslf.usermodel.HSLFSlideShow;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import com.wykay.officedocuments.ExcelDocumentConverter.CallableXlsxByteArrayToPDFByteArray;

public class PowerpointDocumentConverter {
	
	public void convertPptToPDF(String i_doc_path, String i_output_path) throws Exception
	{
		byte[] filebuf = getPDFByteArrayFromPpt(i_doc_path);
		FileOutputStream fileout = new FileOutputStream(new File(i_output_path));
		fileout.write(filebuf);
		fileout.close();
		fileout = null;
	}
	
	public void convertPptxToPDF(String i_doc_path, String i_output_path) throws Exception
	{
		byte[] filebuf = getPDFByteArrayFromPptx(i_doc_path);
		FileOutputStream fileout = new FileOutputStream(new File(i_output_path));
		fileout.write(filebuf);
		fileout.close();
		fileout = null;
	}
	
	public byte[] getPDFByteArrayFromPpt(String i_doc_path) throws Exception
	{
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(i_doc_path));
		HSLFSlideShow hslfSlideshow = new HSLFSlideShow(fs);
		
		Dimension pgsize = hslfSlideshow.getPageSize();
		
		//create rudimentary xsl fo
		StringBuilder fop_gen = new StringBuilder();
		fop_gen.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"> <fo:layout-master-set> ");
		
		int pagesetupcounter=1;
		for (HSLFSlide slide : hslfSlideshow.getSlides())
		{
			fop_gen.append(" <fo:simple-page-master master-name=");
			if (pgsize.width > pgsize.height)
				fop_gen.append("\"A4-landscape-"+pagesetupcounter+ "\" page-height=\"21cm\" page-width=\"29.7cm\">");
			else
				fop_gen.append("\"A4-portrait-" +pagesetupcounter+ "\" page-height=\"29.7cm\" page-width=\"21cm\">");
			fop_gen.append("<fo:region-body region-name=\"region-" + pagesetupcounter + "\"> </fo:region-body> </fo:simple-page-master>");
			++pagesetupcounter;
		}
		
		fop_gen.append("</fo:layout-master-set> ");
		
		//use java awt to capture image
		int idx = 1;
	    for (HSLFSlide slide : hslfSlideshow.getSlides()) {

	        BufferedImage img = new BufferedImage(pgsize.width, pgsize.height, BufferedImage.TYPE_INT_RGB);
	        Graphics2D graphics = img.createGraphics();
	        // clear the drawing area
	        graphics.setPaint(Color.white);
	        graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));

	        // render
	        slide.draw(graphics);

	        // save the output
	        //FileOutputStream out = new FileOutputStream("slide-" + idx + ".png");
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        //javax.imageio.ImageIO.write(img, "png", out);
	        javax.imageio.ImageIO.write(img, "jpg", out);
	        //javax.imageio.ImageIO.write(img, "jpeg", new File("c:\\temp\\ppttest.jpg"));
	        	        	        
	        //add image as base64 encoded data to fop
	        
			
			//add the image
	      //String base64img = Base64.encodeBase64URLSafeString(out.toByteArray());
	        //String base64img = new String(java.util.Base64.getEncoder().encode(out.toByteArray()),"UTF-8");
	        String base64img = DatatypeConverter.printBase64Binary(out.toByteArray());
			
			
			fop_gen.append("<fo:page-sequence master-reference=\"A4-landscape-");
			fop_gen.append(idx);
			//fop_gen.append("\"> <fo:static-content flow-name=\"region-");
			//fop_gen.append(idx);
			fop_gen.append("\"> <fo:flow flow-name=\"region-");
			fop_gen.append(idx);
			//fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
			fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
			fop_gen.append(base64img);
			fop_gen.append("')\"/> </fo:block> </fo:flow> </fo:page-sequence>");
			
			System.out.printf("Image size:%d%n",
	                base64img.length());
	        
	        out.close();
	        out = null;
	        img = null;

	        idx++;
	    }
		
		fop_gen.append("</fo:root>");
		
		//convert to pdf
	    
	    ByteArrayInputStream fopbyte = new ByteArrayInputStream(fop_gen.toString().getBytes(Charset.defaultCharset()));
	    
	    FopFactory fopFactory = null;
    	
	    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
	    
	    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
	    else
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
	    
	    OutputStream out = null;
	    //String savedfilepath = i_doc_path + ".pdf";
	    
	    //Step 2: setup stream, byte if it should not be saved, or fileout if it should
	    
	    	//out = new BufferedOutputStream(new FileOutputStream(new File(savedfilepath)));
	    
	    	out = new ByteArrayOutputStream();
	    

	    // Step 3: Construct fop with desired output format
	    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
	    

	    // Step 4: Setup JAXP using identity transformer
	    TransformerFactory factory = TransformerFactory.newInstance();
	    Transformer transformer = factory.newTransformer(); // identity transformer

	    // Step 5: Setup input and output for XSLT transformation
	    // Setup input stream
	    
	    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
	    Source src = new StreamSource(fopbyte);

	    // Resulting SAX events (the generated FO) must be piped through to FOP
	    Result res = new SAXResult(fop.getDefaultHandler());

	    // Step 6: Start XSLT transformation and FOP processing
	    transformer.transform(src, res);
	    
	    out.close();
		hslfSlideshow.close();
		System.out.println("Finished output");
		res = null;
		src = null;
		transformer = null;
		factory = null;
		fop = null;
		fopFactory = null;
		fopbyte = null;
		fop_gen = null;
		hslfSlideshow = null;
		
	    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
	    return output.toByteArray();
	    
	
	
	}
	
	public byte[] getPDFByteArrayFromPptByteArray(byte[] i_byte_array) throws Exception
	{
		byte[] output = null;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<byte[]> future = executor.submit(new CallablePptByteArrayToPDFByteArray(i_byte_array));
		try
		{
			output = future.get(120, TimeUnit.SECONDS);
		}
		catch(TimeoutException e)
		{
			System.out.println("getPDFByteArrayFromPptByteArray timed out");
			future.cancel(true);
		}
		
		executor.shutdownNow();
		return output;
	    
	
	
	}
	
	public byte[] getPDFByteArrayFromPptx(String i_doc_path) throws Exception
	{
		
		XMLSlideShow xmlslideshow = new XMLSlideShow(new FileInputStream(i_doc_path));
		
		Dimension pgsize = xmlslideshow.getPageSize();
		
		//create rudimentary xsl fo
		StringBuilder fop_gen = new StringBuilder();
		fop_gen.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"> <fo:layout-master-set> ");
		
		int pagesetupcounter=1;
		for (XSLFSlide slide : xmlslideshow.getSlides())
		{
			fop_gen.append(" <fo:simple-page-master master-name=");
			if (pgsize.width > pgsize.height)
				fop_gen.append("\"A4-landscape-"+pagesetupcounter+ "\" page-height=\"21cm\" page-width=\"29.7cm\">");
			else
				fop_gen.append("\"A4-portrait-" +pagesetupcounter+ "\" page-height=\"29.7cm\" page-width=\"21cm\">");
			fop_gen.append("<fo:region-body region-name=\"region-" + pagesetupcounter + "\"> </fo:region-body> </fo:simple-page-master>");
			++pagesetupcounter;
		}
		
		fop_gen.append("</fo:layout-master-set> ");
		
		//use java awt to capture image
		int idx = 1;
	    for (XSLFSlide slide : xmlslideshow.getSlides()) {

	        BufferedImage img = new BufferedImage(pgsize.width, pgsize.height, BufferedImage.TYPE_INT_RGB);
	        Graphics2D graphics = img.createGraphics();
	        // clear the drawing area
	        graphics.setPaint(Color.white);
	        graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));

	        // render
	        slide.draw(graphics);

	        // save the output
	        //FileOutputStream out = new FileOutputStream("slide-" + idx + ".png");
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        //javax.imageio.ImageIO.write(img, "png", out);
	        javax.imageio.ImageIO.write(img, "jpg", out);
	        //javax.imageio.ImageIO.write(img, "jpeg", new File("c:\\temp\\ppttest.jpg"));
	        	        	        
	        //add image as base64 encoded data to fop
	        
			
			//add the image
			//String base64img = Base64.encodeBase64URLSafeString(out.toByteArray());
	        //String base64img = new String(java.util.Base64.getEncoder().encode(out.toByteArray()),"UTF-8");
			String base64img = DatatypeConverter.printBase64Binary(out.toByteArray());
			
			fop_gen.append("<fo:page-sequence master-reference=\"A4-landscape-");
			fop_gen.append(idx);
			//fop_gen.append("\"> <fo:static-content flow-name=\"region-");
			//fop_gen.append(idx);
			fop_gen.append("\"> <fo:flow flow-name=\"region-");
			fop_gen.append(idx);
			//fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
			fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
			fop_gen.append(base64img);
			fop_gen.append("')\"/> </fo:block> </fo:flow> </fo:page-sequence>");
			
			System.out.printf("Image size:%d%n",
	                base64img.length());
	        
	        out.close();
	        img = null;
	        out = null;

	        idx++;
	    }
		
		fop_gen.append("</fo:root>");
		
		//convert to pdf
	    
	    ByteArrayInputStream fopbyte = new ByteArrayInputStream(fop_gen.toString().getBytes(Charset.defaultCharset()));
	    
	    FopFactory fopFactory = null;
    	
	    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
	    
	    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
	    else
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
	    
	    OutputStream out = null;	    
	    
	    //Step 2: setup stream, byte if it should not be saved, or fileout if it should
	    	   
	    
	    out = new ByteArrayOutputStream();
	    

	    // Step 3: Construct fop with desired output format
	    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
	    

	    // Step 4: Setup JAXP using identity transformer
	    TransformerFactory factory = TransformerFactory.newInstance();
	    Transformer transformer = factory.newTransformer(); // identity transformer

	    // Step 5: Setup input and output for XSLT transformation
	    // Setup input stream
	    
	    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
	    Source src = new StreamSource(fopbyte);

	    // Resulting SAX events (the generated FO) must be piped through to FOP
	    Result res = new SAXResult(fop.getDefaultHandler());

	    // Step 6: Start XSLT transformation and FOP processing
	    transformer.transform(src, res);
	    
	    out.close();
		xmlslideshow.close();
		System.out.println("Finished output");	
		res = null;
		src = null;
		transformer = null;
		factory = null;
		fop = null;
		fopFactory = null;
		fopbyte = null;
		fop_gen = null;
		xmlslideshow = null;
		
		
	    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
	    return output.toByteArray();
	    
	}
	
	public byte[] getPDFByteArrayFromPptxByteArray(byte[] i_byte_array) throws Exception
	{
		
		byte[] output = null;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<byte[]> future = executor.submit(new CallablePptxByteArrayToPDFByteArray(i_byte_array));
		try
		{
			output = future.get(120, TimeUnit.SECONDS);
		}
		catch(TimeoutException e)
		{
			System.out.println("getPDFByteArrayFromPptxByteArray timed out");
			future.cancel(true);
		}
		
		executor.shutdownNow();
		return output;
	    
	}
	
	class CallablePptByteArrayToPDFByteArray implements Callable<byte[]>
	{
		byte[] m_input;
		public CallablePptByteArrayToPDFByteArray(byte[] i_input)
		{
			m_input = i_input;
		}
		
		@Override
		public byte[] call() throws Exception
		{
			POIFSFileSystem fs = new POIFSFileSystem(new ByteArrayInputStream(m_input));
			HSLFSlideShow hslfSlideshow = new HSLFSlideShow(fs);
			
			Dimension pgsize = hslfSlideshow.getPageSize();
			
			//create rudimentary xsl fo
			StringBuilder fop_gen = new StringBuilder();
			fop_gen.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"> <fo:layout-master-set> ");
			
			int pagesetupcounter=1;
			for (HSLFSlide slide : hslfSlideshow.getSlides())
			{
				fop_gen.append(" <fo:simple-page-master master-name=");
				if (pgsize.width > pgsize.height)
					fop_gen.append("\"A4-landscape-"+pagesetupcounter+ "\" page-height=\"21cm\" page-width=\"29.7cm\">");
				else
					fop_gen.append("\"A4-portrait-" +pagesetupcounter+ "\" page-height=\"29.7cm\" page-width=\"21cm\">");
				fop_gen.append("<fo:region-body region-name=\"region-" + pagesetupcounter + "\"> </fo:region-body> </fo:simple-page-master>");
				++pagesetupcounter;
			}
			
			fop_gen.append("</fo:layout-master-set> ");
			
			//use java awt to capture image
			int idx = 1;
		    for (HSLFSlide slide : hslfSlideshow.getSlides()) {

		        BufferedImage img = new BufferedImage(pgsize.width, pgsize.height, BufferedImage.TYPE_INT_RGB);
		        Graphics2D graphics = img.createGraphics();
		        // clear the drawing area
		        graphics.setPaint(Color.white);
		        graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));

		        // render
		        slide.draw(graphics);

		        // save the output
		        //FileOutputStream out = new FileOutputStream("slide-" + idx + ".png");
		        ByteArrayOutputStream out = new ByteArrayOutputStream();
		        //javax.imageio.ImageIO.write(img, "png", out);
		        javax.imageio.ImageIO.write(img, "jpg", out);
		        //javax.imageio.ImageIO.write(img, "jpeg", new File("c:\\temp\\ppttest.jpg"));
		        	        	        
		        //add image as base64 encoded data to fop
		        
				
				//add the image
		      //String base64img = Base64.encodeBase64URLSafeString(out.toByteArray());
		        //String base64img = new String(java.util.Base64.getEncoder().encode(out.toByteArray()),"UTF-8");
		        String base64img = DatatypeConverter.printBase64Binary(out.toByteArray());
				
				
				fop_gen.append("<fo:page-sequence master-reference=\"A4-landscape-");
				fop_gen.append(idx);
				//fop_gen.append("\"> <fo:static-content flow-name=\"region-");
				//fop_gen.append(idx);
				fop_gen.append("\"> <fo:flow flow-name=\"region-");
				fop_gen.append(idx);
				//fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
				fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
				fop_gen.append(base64img);
				fop_gen.append("')\"/> </fo:block> </fo:flow> </fo:page-sequence>");
				
				System.out.printf("Image size:%d%n",
		                base64img.length());
		        
		        out.close();
		        out = null;
		        img = null;

		        idx++;
		    }
			
			fop_gen.append("</fo:root>");
			
			//convert to pdf
		    
		    ByteArrayInputStream fopbyte = new ByteArrayInputStream(fop_gen.toString().getBytes(Charset.defaultCharset()));
		    
		    FopFactory fopFactory = null;
	    	
		    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
		    
		    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
		    else
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
		    
		    OutputStream out = null;
		    //String savedfilepath = i_doc_path + ".pdf";
		    
		    //Step 2: setup stream, byte if it should not be saved, or fileout if it should
		    
		    	//out = new BufferedOutputStream(new FileOutputStream(new File(savedfilepath)));
		    
		    	out = new ByteArrayOutputStream();
		    

		    // Step 3: Construct fop with desired output format
		    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
		    

		    // Step 4: Setup JAXP using identity transformer
		    TransformerFactory factory = TransformerFactory.newInstance();
		    Transformer transformer = factory.newTransformer(); // identity transformer

		    // Step 5: Setup input and output for XSLT transformation
		    // Setup input stream
		    
		    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
		    Source src = new StreamSource(fopbyte);

		    // Resulting SAX events (the generated FO) must be piped through to FOP
		    Result res = new SAXResult(fop.getDefaultHandler());

		    // Step 6: Start XSLT transformation and FOP processing
		    transformer.transform(src, res);
		    
		    out.close();
			hslfSlideshow.close();
			System.out.println("Finished output");
			res = null;
			src = null;
			transformer = null;
			factory = null;
			fop = null;
			fopFactory = null;
			fopbyte = null;
			fop_gen = null;
			hslfSlideshow = null;
			
		    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
		    return output.toByteArray();
		}
	}
	
	class CallablePptxByteArrayToPDFByteArray implements Callable<byte[]>
	{
		byte[] m_input;
		public CallablePptxByteArrayToPDFByteArray(byte[] i_input)
		{
			m_input = i_input;
		}
		
		@Override
		public byte[] call() throws Exception
		{
			XMLSlideShow xmlslideshow = new XMLSlideShow(new ByteArrayInputStream(m_input));
			
			Dimension pgsize = xmlslideshow.getPageSize();
			
			//create rudimentary xsl fo
			StringBuilder fop_gen = new StringBuilder();
			fop_gen.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"> <fo:layout-master-set> ");
			
			int pagesetupcounter=1;
			for (XSLFSlide slide : xmlslideshow.getSlides())
			{
				fop_gen.append(" <fo:simple-page-master master-name=");
				if (pgsize.width > pgsize.height)
					fop_gen.append("\"A4-landscape-"+pagesetupcounter+ "\" page-height=\"21cm\" page-width=\"29.7cm\">");
				else
					fop_gen.append("\"A4-portrait-" +pagesetupcounter+ "\" page-height=\"29.7cm\" page-width=\"21cm\">");
				fop_gen.append("<fo:region-body region-name=\"region-" + pagesetupcounter + "\"> </fo:region-body> </fo:simple-page-master>");
				++pagesetupcounter;
			}
			
			fop_gen.append("</fo:layout-master-set> ");
			
			//use java awt to capture image
			int idx = 1;
		    for (XSLFSlide slide : xmlslideshow.getSlides()) {

		        BufferedImage img = new BufferedImage(pgsize.width, pgsize.height, BufferedImage.TYPE_INT_RGB);
		        Graphics2D graphics = img.createGraphics();
		        // clear the drawing area
		        graphics.setPaint(Color.white);
		        graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));

		        // render
		        slide.draw(graphics);

		        // save the output
		        //FileOutputStream out = new FileOutputStream("slide-" + idx + ".png");
		        ByteArrayOutputStream out = new ByteArrayOutputStream();
		        //javax.imageio.ImageIO.write(img, "png", out);
		        javax.imageio.ImageIO.write(img, "jpg", out);
		        //javax.imageio.ImageIO.write(img, "jpeg", new File("c:\\temp\\ppttest.jpg"));
		        	        	        
		        //add image as base64 encoded data to fop
		        
				
				//add the image
				//String base64img = Base64.encodeBase64URLSafeString(out.toByteArray());
		        //String base64img = new String(java.util.Base64.getEncoder().encode(out.toByteArray()),"UTF-8");
				String base64img = DatatypeConverter.printBase64Binary(out.toByteArray());
				
				fop_gen.append("<fo:page-sequence master-reference=\"A4-landscape-");
				fop_gen.append(idx);
				//fop_gen.append("\"> <fo:static-content flow-name=\"region-");
				//fop_gen.append(idx);
				fop_gen.append("\"> <fo:flow flow-name=\"region-");
				fop_gen.append(idx);
				//fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
				fop_gen.append("\"> <fo:block text-align=\"center\"> <fo:external-graphic content-width=\"scale-to-fit\" content-height=\"100%\" src=\"url('data:image/jpeg;base64,");
				fop_gen.append(base64img);
				fop_gen.append("')\"/> </fo:block> </fo:flow> </fo:page-sequence>");
				
				System.out.printf("Image size:%d%n",
		                base64img.length());
		        
		        out.close();
		        out = null;
		        img = null;

		        idx++;
		    }
			
			fop_gen.append("</fo:root>");
			
			//convert to pdf
		    
		    ByteArrayInputStream fopbyte = new ByteArrayInputStream(fop_gen.toString().getBytes(Charset.defaultCharset()));
		    
		    FopFactory fopFactory = null;
	    	
		    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
		    
		    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
		    else
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
		    
		    OutputStream out = null;	    
		    
		    //Step 2: setup stream, byte if it should not be saved, or fileout if it should
		    	   
		    
		    out = new ByteArrayOutputStream();
		    

		    // Step 3: Construct fop with desired output format
		    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
		    

		    // Step 4: Setup JAXP using identity transformer
		    TransformerFactory factory = TransformerFactory.newInstance();
		    Transformer transformer = factory.newTransformer(); // identity transformer

		    // Step 5: Setup input and output for XSLT transformation
		    // Setup input stream
		    
		    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
		    Source src = new StreamSource(fopbyte);

		    // Resulting SAX events (the generated FO) must be piped through to FOP
		    Result res = new SAXResult(fop.getDefaultHandler());

		    // Step 6: Start XSLT transformation and FOP processing
		    transformer.transform(src, res);
		    
		    out.close();
			xmlslideshow.close();
			System.out.println("Finished output");	
			res = null;
			src = null;
			transformer = null;
			factory = null;
			fop = null;
			fopFactory = null;
			fopbyte = null;
			fop_gen = null;
			xmlslideshow = null;
			
			
		    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
		    return output.toByteArray();
		}
	}


}
