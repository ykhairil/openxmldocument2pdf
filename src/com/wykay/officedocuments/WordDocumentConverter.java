package com.wykay.officedocuments;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.converter.WordToFoConverter;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.util.XMLHelper;
import org.docx4j.Docx4J;
import org.docx4j.convert.out.FOSettings;
import org.docx4j.fonts.IdentityPlusMapper;
import org.docx4j.fonts.Mapper;
import org.docx4j.fonts.PhysicalFont;
import org.docx4j.fonts.PhysicalFonts;
import org.docx4j.model.fields.FieldUpdater;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.wykay.officedocuments.ExcelDocumentConverter.CallableXlsxByteArrayToPDFByteArray;



public class WordDocumentConverter {	
	
	public void convertDocToPDF(String i_doc_path, String i_output_path) throws Exception
	{
		byte[] filebuf = getPDFByteArrayFromDoc(i_doc_path);
		FileOutputStream fileout = new FileOutputStream(new File(i_output_path));
		fileout.write(filebuf);
		fileout.close();
		fileout = null;
	}
	
	public void convertDocxToPDF(String i_doc_path, String i_output_path) throws Exception
	{
		byte[] filebuf = getPDFByteArrayFromDocx(i_doc_path);
		FileOutputStream fileout = new FileOutputStream(new File(i_output_path));
		fileout.write(filebuf);
		fileout.close();
		fileout = null;
	}
	
	public byte[] getPDFByteArrayFromDoc(String i_doc_path) throws Exception
	{		
		//Step 1: get FOP structure
		//HWPFDocument hwpfDocument = new HWPFDocument(new FileInputStream(i_doc_path));
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(i_doc_path));
		HWPFDocument hwpfDocument = new HWPFDocument(fs);

	    WordToFoConverter wordToFoConverter = new WordToFoConverter(XMLHelper.getDocumentBuilderFactory().newDocumentBuilder().newDocument());
	    wordToFoConverter.processDocument(hwpfDocument);

	    StringWriter stringWriter = new StringWriter();

	    Transformer transformer = TransformerFactory.newInstance().newTransformer();
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.transform(new DOMSource(wordToFoConverter.getDocument()), new StreamResult(stringWriter));

	    String xslt_result = stringWriter.toString();   
	    
	    //convert to pdf
	    
	    ByteArrayInputStream fopbyte = new ByteArrayInputStream(xslt_result.getBytes(Charset.defaultCharset()));
	    
	    FopFactory fopFactory = null;
    	
	    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
	    
	    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
	    else
	    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
	    
	    OutputStream out = null;
	    
	    	out = new ByteArrayOutputStream();
	    	
	    

	 // Step 3: Construct fop with desired output format
	    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);

	    // Step 4: Setup JAXP using identity transformer
	    //TransformerFactory factory = TransformerFactory.newInstance();
	    //Transformer transformer = factory.newTransformer(); // identity transformer

	    // Step 5: Setup input and output for XSLT transformation
	    // Setup input stream
	    
	    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
	    Source src = new StreamSource(fopbyte);

	    // Resulting SAX events (the generated FO) must be piped through to FOP
	    Result res = new SAXResult(fop.getDefaultHandler());

	    // Step 6: Start XSLT transformation and FOP processing
	    transformer.transform(src, res);
	    
	    out.close();
	    
	    System.out.println("Finished output");
	    res = null;
	    src = null;
	    fopFactory = null;
	    fopbyte = null;
	    transformer = null;
	    stringWriter = null;
	    wordToFoConverter = null;
	    hwpfDocument = null;
	    fs = null;
	   
	    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
	    return output.toByteArray();
	  
	}
	
	public byte[] getPDFByteArrayFromDocByteArray(byte[] i_byte_array) throws Exception
	{		
		byte[] output = null;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<byte[]> future = executor.submit(new CallableDocByteArrayToPDFByteArray(i_byte_array));
		try
		{
			output = future.get(120, TimeUnit.SECONDS);
		}
		catch(TimeoutException e)
		{
			System.out.println("getPDFByteArrayFromDocByteArray timed out");
			future.cancel(true);
		}
		
		executor.shutdownNow();
		return output;
	  
	}
	
	
	
	public byte[] getPDFByteArrayFromDocx(String i_doc_path) throws Exception
	{
			
		
		boolean saveFO=false;
		// Font regex (optional)
		// Set regex if you want to restrict to some defined subset of fonts
		// Here we have to do this before calling createContent,
		// since that discovers fonts
		String regex = null;
		// Windows:
		// String
		// regex=".*(calibri|camb|cour|arial|symb|times|Times|zapf).*";
		//regex=".*(calibri|camb|cour|arial|times|comic|georgia|impact|LSANS|pala|tahoma|trebuc|verdana|symbol|webdings|wingding).*";
		// Mac
		// String
		// regex=".*(Courier New|Arial|Times New Roman|Comic Sans|Georgia|Impact|Lucida Console|Lucida Sans Unicode|Palatino Linotype|Tahoma|Trebuchet|Verdana|Symbol|Webdings|Wingdings|MS Sans Serif|MS Serif).*";
		PhysicalFonts.setRegex(regex);
		
		WordprocessingMLPackage wordMLPackage = null;		
		wordMLPackage = Docx4J.load(new java.io.File(i_doc_path));
		
		
		
		// Refresh the values of DOCPROPERTY fields 
		FieldUpdater updater = new FieldUpdater(wordMLPackage);		
		updater.update(true);
		

		String outputfilepath;
		if (i_doc_path==null) {
			outputfilepath = System.getProperty("user.dir") + "/OUT_FontContent.pdf";			
		} else {
			outputfilepath = i_doc_path + ".pdf";
		}
		
		// output to an OutputStream.		
		OutputStream out = null; 
		//if (save) {			
		//		out = new FileOutputStream(outputfilepath);
			
		//} else {
			out = new ByteArrayOutputStream();
			
		//}
		
		// Set up font mapper (optional)
		Mapper fontMapper = new IdentityPlusMapper();
		
		wordMLPackage.setFontMapper(fontMapper);
		
		PhysicalFont font = PhysicalFonts.get("Arial Unicode MS"); 
		FOSettings foSettings = Docx4J.createFOSettings();
		if (saveFO) 
		{
			foSettings.setFoDumpFile(new java.io.File(i_doc_path + ".fo"));
		}
		foSettings.setWmlPackage(wordMLPackage);
		
		Docx4J.toFO(foSettings, out, Docx4J.FLAG_EXPORT_PREFER_XSL);
		
		//System.out.println("Saved: " + outputfilepath);

		// Clean up, so any ObfuscatedFontPart temp files can be deleted 
		if (wordMLPackage.getMainDocumentPart().getFontTablePart()!=null) {
			wordMLPackage.getMainDocumentPart().getFontTablePart().deleteEmbeddedFontTempFiles();
		}		
		// This would also do it, via finalize() methods
		
		System.out.println("Finished output");
		updater = null;
		foSettings = null;
		wordMLPackage = null;
		fontMapper = null;
		
		//if (i_do_save)
	    //{
	    	ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
	    	return output.toByteArray();
	    //}
	    //else
	    //	return null;
		
	}
	
	public byte[] getPDFByteArrayFromDocxByteArray(byte[] i_byte_array) throws Exception
	{
		byte[] output = null;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<byte[]> future = executor.submit(new CallableDocxByteArrayToPDFByteArray(i_byte_array));
		try
		{
			output = future.get(120, TimeUnit.SECONDS);
		}
		catch(TimeoutException e)
		{
			System.out.println("getPDFByteArrayFromDocxByteArray timed out");
			future.cancel(true);
		}
		
		executor.shutdownNow();
		return output;
		
	}
	
	class CallableDocxByteArrayToPDFByteArray implements Callable<byte[]>
	{
		byte[] m_input;
		
		public CallableDocxByteArrayToPDFByteArray(byte[] i_input)
		{
			m_input = i_input ;
		}
		
		@Override
		public byte[] call() throws Exception
		{
			ByteArrayInputStream docbyte = new ByteArrayInputStream(m_input);	
			
			boolean saveFO=false;
			// Font regex (optional)
			// Set regex if you want to restrict to some defined subset of fonts
			// Here we have to do this before calling createContent,
			// since that discovers fonts
			String regex = null;
			// Windows:
			// String
			// regex=".*(calibri|camb|cour|arial|symb|times|Times|zapf).*";
			//regex=".*(calibri|camb|cour|arial|times|comic|georgia|impact|LSANS|pala|tahoma|trebuc|verdana|symbol|webdings|wingding).*";
			// Mac
			// String
			// regex=".*(Courier New|Arial|Times New Roman|Comic Sans|Georgia|Impact|Lucida Console|Lucida Sans Unicode|Palatino Linotype|Tahoma|Trebuchet|Verdana|Symbol|Webdings|Wingdings|MS Sans Serif|MS Serif).*";
			PhysicalFonts.setRegex(regex);
			
			WordprocessingMLPackage wordMLPackage = null;		
			wordMLPackage = Docx4J.load(docbyte);
			
			
			
			// Refresh the values of DOCPROPERTY fields 
			FieldUpdater updater = new FieldUpdater(wordMLPackage);		
			updater.update(true);		
			
			
			// output to an OutputStream.		
			OutputStream out = null; 
			//if (save) {			
			//		out = new FileOutputStream(outputfilepath);
				
			//} else {
				out = new ByteArrayOutputStream();
				
			//}
			
			// Set up font mapper (optional)
			Mapper fontMapper = new IdentityPlusMapper();
			
			wordMLPackage.setFontMapper(fontMapper);
			
			PhysicalFont font = PhysicalFonts.get("Arial Unicode MS"); 
			FOSettings foSettings = Docx4J.createFOSettings();
			
			foSettings.setWmlPackage(wordMLPackage);
			
			Docx4J.toFO(foSettings, out, Docx4J.FLAG_EXPORT_PREFER_XSL);
			
			//System.out.println("Saved: " + outputfilepath);

			// Clean up, so any ObfuscatedFontPart temp files can be deleted 
			if (wordMLPackage.getMainDocumentPart().getFontTablePart()!=null) {
				wordMLPackage.getMainDocumentPart().getFontTablePart().deleteEmbeddedFontTempFiles();
			}		
			// This would also do it, via finalize() methods
			
			System.out.println("Finished output");
			updater = null;
			foSettings = null;
			wordMLPackage = null;
			fontMapper = null;
			//if (i_do_save)
		    //{
		    	ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
		    	return output.toByteArray();
		    //}
		    //else
		    //	return null;
		}
	}
	
	class CallableDocByteArrayToPDFByteArray implements Callable<byte[]>
	{
		byte[] m_input;
		
		public CallableDocByteArrayToPDFByteArray(byte[] i_input)
		{
			m_input = i_input ;
		}
		
		@Override
		public byte[] call() throws Exception
		{
			//Step 1: get FOP structure
			//HWPFDocument hwpfDocument = new HWPFDocument(new FileInputStream(i_doc_path));
			
			ByteArrayInputStream docbyte = new ByteArrayInputStream(m_input);
			POIFSFileSystem fs = new POIFSFileSystem(docbyte);
			HWPFDocument hwpfDocument = new HWPFDocument(fs);

		    WordToFoConverter wordToFoConverter = new WordToFoConverter(XMLHelper.getDocumentBuilderFactory().newDocumentBuilder().newDocument());
		    wordToFoConverter.processDocument(hwpfDocument);

		    StringWriter stringWriter = new StringWriter();

		    Transformer transformer = TransformerFactory.newInstance().newTransformer();
		    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		    transformer.transform(new DOMSource(wordToFoConverter.getDocument()), new StreamResult(stringWriter));

		    String xslt_result = stringWriter.toString();   
		    
		    //convert to pdf
		    
		    ByteArrayInputStream fopbyte = new ByteArrayInputStream(xslt_result.getBytes(Charset.defaultCharset()));
		    
		    FopFactory fopFactory = null;
	    	
		    System.out.printf("Host OS :%s%n",  System.getProperty("os.name"));
		    
		    if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0)
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop_osx.xconf"));
		    else
		    	fopFactory = FopFactory.newInstance(new File(".").toURI(), this.getClass().getResourceAsStream("fop.xconf"));
		    
		    OutputStream out = null;
		    
		    	out = new ByteArrayOutputStream();
		    	
		    

		 // Step 3: Construct fop with desired output format
		    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);

		    // Step 4: Setup JAXP using identity transformer
		    //TransformerFactory factory = TransformerFactory.newInstance();
		    //Transformer transformer = factory.newTransformer(); // identity transformer

		    // Step 5: Setup input and output for XSLT transformation
		    // Setup input stream
		    
		    //Source src = new StreamSource(new File(path.toString() + "\\" + path.getFileName() + ".fo"));
		    Source src = new StreamSource(fopbyte);

		    // Resulting SAX events (the generated FO) must be piped through to FOP
		    Result res = new SAXResult(fop.getDefaultHandler());

		    // Step 6: Start XSLT transformation and FOP processing
		    transformer.transform(src, res);
		    
		    out.close();
		    
		    System.out.println("Finished output");
		    res = null;
		    src = null;
		    fop = null;
		    fopbyte = null;
		    stringWriter = null;
		    wordToFoConverter=null;
		    hwpfDocument = null;
		    fs = null;
		    docbyte = null;
		    
		   
		    ByteArrayOutputStream output = (ByteArrayOutputStream)out;		
		    return output.toByteArray();
		}
	}
	
	
	
}
